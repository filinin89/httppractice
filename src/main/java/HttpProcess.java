import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class HttpProcess {

    private HttpURLConnection connection;
    private String entity= "";
    private String extractedCsrf;
    private String extractedYandexuid;
    private String toponym;
    private String coordinates;
    private Logger logger = LoggerFactory.getLogger(HttpProcess.class);



    public void connecting() throws IOException {
        URL url = new URL("https://yandex.ru/maps/44/izhevsk");
        connection = (HttpURLConnection) url.openConnection(); // получаем connection
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
        connection.setRequestMethod("GET");
        entity  = IOUtils.toString(connection.getInputStream());
        logger.info(entity);
    }

    public void findCsrfToken(){
        int startIndex = entity.indexOf("\"csrfToken\":\"") +13;
        int endIndex =  entity.indexOf("\",", startIndex);
        extractedCsrf = entity.substring(startIndex, endIndex);
        logger.info("Csrf token: {}", extractedCsrf);
    }

    public void findYandexUid() throws IOException {
        //Map<String, List<String>> headerFields = connection.getHeaderFields(); // получим http headers
        int startIndex = entity.indexOf("yandexuid=");
        int endIndex = entity.indexOf("\"/", startIndex);
        extractedYandexuid = entity.substring(startIndex, endIndex);
        logger.info(extractedYandexuid);
    }


    public void request2() throws IOException {
        URL url = new URL("https://yandex.ru/maps/api/search?text=".concat(URLEncoder.encode("Москва, Кремль", "UTF-8").concat("&lang=ru_RU&csrfToken=") + extractedCsrf)); // Москва, Кремль   Ижевск, Вадима Сивкова, 150
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Cookie", extractedYandexuid);
        toponym = IOUtils.toString(connection.getInputStream()); // можно сделать через InputStreamReader
    }


    public void findCoordinates(){
        int startIndex = toponym.indexOf("\"coordinates\":[");
        int endIndex = toponym.indexOf("]", startIndex);
        coordinates = toponym.substring(startIndex + 15, endIndex);
        logger.info("Найденные координаты: {}", coordinates);
        String[] coordRev = coordinates.split(",");
        logger.info("Перевернутые координаты: {},{}",  coordRev[1], coordRev[0]);

    }

}
