import org.apache.commons.io.IOUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        HttpProcess httpProcess = new HttpProcess();
        httpProcess.connecting();

        // 1. найдем токен
        httpProcess.findCsrfToken();

        // 2. найдем yandexuid
        httpProcess.findYandexUid();

        // 3. запрос с передачей полученных ранее значений
        httpProcess.request2();

        //4. найдем координаты
        httpProcess.findCoordinates();




















    }
}
